%global _default_patch_fuzz 2

# Don't build pyo and pyc Python files
%define __os_install_post %{nil}

#% define tag_release rc.2
%define tag_release 1

Summary: ClearGLASS Community cloud management platform
Name: clearglass-community
Version: 3.3.0
Release: 6.%{tag_release}%{?dist}
License: AGPLv3
Group: System Environment/Base
URL: http://www.clear.glass
BuildArch: noarch
Source: %{name}-%{version}.tar.gz
Source1: clearglass-restart
Source2: clearglass.service
Source3: 20-clearglass
Source4: clearglass-postfix
Patch0: clearglass-community-v3.0.0-docker-compose.patch
Provides: clearglass-engine
BuildRequires: sed
BuildRequires: systemd
%{?systemd_requires}
Requires: docker
Requires: docker-compose
Requires: util-linux

%description
ClearGLASS Community cloud management platform.

%prep
%setup -q
%patch0 -p0

%build
# Sanity check manual download - Layer 8 check.  Cheating with using periods in grep expression.
echo "------ Sanity check composer version -----"
#grep "%{version}-%{tag_release}" docker-compose.yml
grep "%{version}" docker-compose.yml

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/var/lib/clearglass/config/ssl/certs
mkdir -p $RPM_BUILD_ROOT/var/lib/clearglass/settings
mkdir -p $RPM_BUILD_ROOT/%{_unitdir}
mkdir -p $RPM_BUILD_ROOT/%{_sbindir}

install -D -m 0755 %SOURCE1 $RPM_BUILD_ROOT%{_sbindir}/clearglass-restart
install -D -m 0644 %SOURCE2 $RPM_BUILD_ROOT/%{_unitdir}/clearglass.service
install -D -m 0755 %SOURCE3 %{buildroot}%{_sysconfdir}/clearos/firewall.d/20-clearglass
install -D -m 0755 %SOURCE4 $RPM_BUILD_ROOT%{_sbindir}/clearglass-postfix

# This is a bit messy

# 1) We are patching the docker-compose.yml file from the GitLab build artifacts
# e.g. https://gitlab.com/clearglass/clearglass-community/tags/v3.0.0-beta.4
cp -a docker-compose.yml $RPM_BUILD_ROOT/var/lib/clearglass/

# 2) ... and adding default settings.conf file.  Usually, this is
# automatically created on a "docker-compose up", but that would leave
# /var/lib/clearglass/settings/settings.py an orphaned file.  To avoid
# orphaning the file, include the default in the RPM.
# In other words, "rpm -e clearglass-community" leaves the
# /var/lib/clearglass directory in a clean state.
cp -a clearglass-community-settings.py $RPM_BUILD_ROOT/var/lib/clearglass/settings/settings.py

# 3) ... and shipping with SSL enabled
cp -a clearglass-community-nginx-listen.conf $RPM_BUILD_ROOT/var/lib/clearglass/config/nginx-listen.conf
cp -a clearglass-community-nginx_certs.conf $RPM_BUILD_ROOT/var/lib/clearglass/config/ssl/nginx_certs.conf

%post
/usr/bin/systemctl daemon-reload >/dev/null 2>&1

%preun
%systemd_preun clearglass.service

%postun
if [ $1 -ge 1 ] ; then
    # A ClearGLASS restart requires downloading from remote servers.
    # That i) might take time and ii) fail.
    # We don't want to yum/rpm waiting around, so do this via a cronjob.
    # Lame, but ...
    systemctl stop clearglass
    echo "* * * * *  root %{_sbindir}/clearglass-restart" > /etc/cron.d/clearglass
fi

exit 0

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%dir /var/lib/clearglass
%dir /var/lib/clearglass/config
%dir /var/lib/clearglass/config/ssl
%dir /var/lib/clearglass/config/ssl/certs
%dir /var/lib/clearglass/settings
%{_sbindir}/clearglass-restart
%{_unitdir}/clearglass.service
/var/lib/clearglass/docker-compose.yml
/var/lib/clearglass/config/nginx-listen.conf
%config(noreplace) /var/lib/clearglass/config/ssl/nginx_certs.conf
%config(noreplace) /var/lib/clearglass/settings/settings.py
%{_sysconfdir}/clearos/firewall.d/20-clearglass
%{_sbindir}/clearglass-postfix
