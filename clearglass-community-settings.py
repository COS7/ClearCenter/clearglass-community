"""User defined settings"""

## Configure external url.
CORE_URI = "https://localhost:9443"

## Verify SSL outgoing SSL connections.
# SSL_VERIFY = True

## Configure email settings.
MAILER_SETTINGS = {
     'mail.host': "172.17.0.1",
     'mail.port': "25",
     'mail.tls': False,
     'mail.starttls': False,
     'mail.username': "",
     'mail.password': "",
}

## Configure whether to serve bundled/minified UI assets.
# JS_BUILD = True

## Allow mist.io to connect to localhost.
# ALLOW_CONNECT_LOCALHOST = True

## Allow mist.io to connect to private IP addresses.
# ALLOW_CONNECT_PRIVATE = True

## API token for github.
# GITHUB_BOT_TOKEN = ""
