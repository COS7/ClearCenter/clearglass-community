#!/bin/bash

function get_firewall_rules()
{
    ${IPTABLES} -nv --line-numbers -t $1 -L $2 | grep $ClearglassIF | awk '{ print $1 }' | sort -rn
}

function delete_firewall_rules()
{
    for chain in $2; do
        RULE_IDS=$(get_firewall_rules $1 $2)
        [ -z "$RULE_IDS" ] && continue

        for rule_id in $RULE_IDS; do
            ${IPTABLES} -t $1 -D $2 ${rule_id}
        done
    done
}

function clear_stale_rules()
{
    # filter table

    table='filter'
    CHAINS="INPUT FORWARD OUTPUT DOCKER DOCKER-ISOLATION"
    for chain in $CHAINS; do
        delete_firewall_rules ${table} ${chain}
    done        

    # nat table

    table='nat'
    CHAINS="POSTROUTING DOCKER"
    for chain in $CHAINS; do
        delete_firewall_rules ${table} ${chain}
    done
}

# Check firewall flag
#--------------------

RUN_HOOK='yes'

if [ -e /etc/clearos/docker.conf ]; then
    CHECK=$(grep -i '^enable_firewall[[:space:]]*=[[:space:]]*no' /etc/clearos/docker.conf 2>/dev/null)
    if [ -n "$CHECK" ]; then
        RUN_HOOK='no'
    fi
fi

# Firewall hook
#--------------

if [ "$RUN_HOOK" == 'yes' ]; then

    # Check the state file exists; if not, initialise the file/parameter
    CHECK=$(grep '^clearglass_interface' /var/clearos/clearglass_community/clearglass.state 2>/dev/null)
    if [ -z "$CHECK" ]; then
        echo 'clearglass_interface = ' >> /var/clearos/clearglass_community/clearglass.state
    fi

    # Check if $IPTABLES is set. This allows the program to run outside control of the firewall.
    # i.e on Clearglass start. If running under firewall control, rules would already be cleared
    #-------------------------------------------------------------------------------------------
    if [ -z "$IPTABLES" ] ; then
        IPTABLES='/usr/sbin/iptables -w'
        FW_PROTO='ipv4'
        ClearglassIF=$(grep '^clearglass_interface' /var/clearos/clearglass_community/clearglass.state | awk '{ print $3 }')
        if [ -n "$ClearglassIF" ]; then
            clear_stale_rules
        fi
    fi

    # This will bail if the script runs as part of the firewall restart and is not ipv4
    if [ "$FW_PROTO" != 'ipv4' ]; then
        return 0
    fi

    sed -i -e 's/^clearglass_interface.*/clearglass_interface = /g' /var/clearos/clearglass_community/clearglass.state
	
    # Now only run if Clearglass is running
    #--------------------------------------
    ps aux | grep clearglass | grep python > /dev/null
    if [ $? -eq 0 ]; then
        # Loop for MaxAttempts retries at 1s interval waiting for the interface to appear
        #--------------------------------------------------------------------------------
        MaxAttempts=60
        counter=1
        while [ $counter -le $MaxAttempts ]; do
            ClearglassNetworkID=$(/usr/bin/docker inspect 'clearglass_ui_1' --format '{{ .NetworkSettings.Networks.clearglass_default.NetworkID }}' 2>/dev/null)
            NewClearglassIF=$(echo 'br-'${ClearglassNetworkID:0:12})
            if [ ${#NewClearglassIF} -ne 15 ]; then    
                ((counter++))
                sleep 1
            else
                # Interface is up. Now add firewall rules
                #----------------------------------------
                ClearglassNetwork=$(ip route | grep $NewClearglassIF | awk '{ print $1 }')
                ${IPTABLES} -A INPUT -i $NewClearglassIF -j ACCEPT
                ${IPTABLES} -A FORWARD -o $NewClearglassIF -j DOCKER
                ${IPTABLES} -A FORWARD -i $NewClearglassIF -j ACCEPT
                ${IPTABLES} -A OUTPUT -o $NewClearglassIF -j ACCEPT
                ${IPTABLES} -A DOCKER-ISOLATION -i $NewClearglassIF -o docker0 -j DROP
                ${IPTABLES} -A DOCKER-ISOLATION -i docker0 -o $NewClearglassIF -j DROP
                ${IPTABLES} -A POSTROUTING -t nat ! -o $NewClearglassIF -s $ClearglassNetwork -j MASQUERADE
                ${IPTABLES} -A DOCKER -t nat -i $NewClearglassIF -j RETURN
                sed -i -e "s/^clearglass_interface.*/clearglass_interface = $NewClearglassIF/g" /var/clearos/clearglass_community/clearglass.state
                logger -t ClearGLASS "Interface came up in $counter seconds."
                break
            fi
        done
        if [ ${#NewClearglassIF} -ne 15 ]; then    
            logger -t ClearGLASS "Interface not up in $MaxAttempts seconds. Consider raising MaxAttempts in /etc/clearos/firewall.d/20-clearglass"
        fi
        
    fi
fi
